//
//  GameViewControllerTest.m
//  Flappy Brid
//
//  Created by Admin on 3/11/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "GameViewController.h"
#import "ViewController.h"

@interface GameViewControllerTest : XCTestCase

@end

@implementation GameViewControllerTest
{
    GameViewController *sut;
    ViewController *view;
}
- (void)setUp {
    [super setUp];
    sut = [[GameViewController alloc]init];
    sut.TunnelTop = [[UIImageView alloc]init];
    sut.TunnelBottom = [[UIImageView alloc]init];
    sut.StartGame = [[UIButton alloc]init];
    sut.Exit = [[UIButton alloc]init];
    sut.Bird = [[UIImageView alloc]init];
    sut.Top = [[UIImageView alloc]init];
    sut.Bottom = [[UIImageView alloc]init];
    [sut viewDidLoad];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testStartGame_Valid_ViewsShouldShowCorrectly{
    [sut StartGame:[sut StartGame]];

    XCTAssertTrue([sut TunnelTop].hidden == NO);
    XCTAssertTrue(sut.TunnelBottom.hidden == NO);
    XCTAssertTrue(sut.StartGame.hidden == YES);
    
}

-(void)testStartGame_Valid_ShouldCallPlaceTunel{
    id mock = OCMPartialMock(sut);
    [sut StartGame:[sut StartGame]];
    OCMVerify([mock PlaceTunnels]);
    
}

-(void)testScore_ScoreNumberHasValue_ReturnExpectedValue{
    sut.ScoreNumber = 3;
    [sut Score];
    //expected value is 4
    XCTAssertEqual(sut.ScoreNumber, 4);
}

-(void)testGameOver_ScoreNumerGreaterThanHighScoreNumber_HighScoreSavedIsNotEqualZero{
    sut.ScoreNumber = 3;
    sut.HighScoreNumber = 0;
    [sut GameOver];
    XCTAssertEqual([[NSUserDefaults standardUserDefaults] integerForKey:@"HighScoreSaved"], 3);
}

-(void)testGameOver_ScoreNumberLesserThanHighScoreNumber_HighScoreSavedIsEqualZero{
    sut.ScoreNumber = 0;
    sut.HighScoreNumber = 0;
    [sut GameOver];
    XCTAssertEqual([[NSUserDefaults standardUserDefaults] integerForKey:@"HighScoreSaved"], 0);
}

-(void)testGameOver_Valid_ViewsShouldShowCorrectly{
    id mockTimerTunnelMovement = OCMClassMock([NSTimer class]);
    id mockTimerBirdMovement = OCMClassMock([NSTimer class]);
    sut.TunnelMovement = mockTimerTunnelMovement;
    sut.BirdMovement = mockTimerBirdMovement;
    [sut GameOver];
    XCTAssertTrue(sut.Exit.hidden == NO);
    XCTAssertTrue(sut.TunnelTop.hidden == YES);
    XCTAssertTrue(sut.TunnelBottom.hidden == YES);
    XCTAssertTrue(sut.Bird.hidden == YES);
 
}

-(void)testGameOver_Valid_TimerShouldStop{
    id mockTimerTunnelMovement = OCMClassMock([NSTimer class]);
    id mockTimerBirdMovement = OCMClassMock([NSTimer class]);
    sut.TunnelMovement = mockTimerTunnelMovement;
    sut.BirdMovement = mockTimerBirdMovement;
    [sut GameOver];
    OCMVerify([mockTimerTunnelMovement invalidate]);
    OCMVerify([mockTimerBirdMovement invalidate]);
}

-(void)testGameOver_Valid_ShouldCallMethodDissmissViewControllerAnimated{
    id mock = OCMPartialMock(sut);
    [sut GameOver];
    OCMVerify([mock dismissViewControllerAnimated:[OCMArg any] completion:nil]);
}

-(void)testTouchesBegan_BirdFlightHasValue_BirdFlightShouldHasValueEqualTo30{
    id mockNSSet = OCMClassMock([NSSet class]);
    id mockEvent = OCMPartialMock([[UIEvent alloc]init]);
    [sut touchesBegan:mockNSSet withEvent:mockEvent];
    XCTAssertEqual(sut.BirdFlight, 30);
}

-(void)testBirdMoving_BirdFlightGreaterThanZero_BirdShouldFlyUp{
    sut.BirdFlight = 10;
    [sut BirdMoving];
    XCTAssertEqualObjects(sut.Bird.image, [UIImage imageNamed:@"BirdUp.png"]);
}

-(void)testBirdMoving_BirdFlightLesserThanZero_BirdSHouldFlyDown{
    sut.BirdFlight = -10;
    [sut BirdMoving];
    XCTAssertEqualObjects(sut.Bird.image, [UIImage imageNamed:@"BirdDown.png"]);
}

-(void)testTunnelMoving_BirdIntersectWithTunnelTop_ShouldCallGameOver{
    CGRect frame = sut.TunnelTop.frame;
    frame.origin.x = 300;
    frame.origin.y = 500;
    sut.TunnelTop.frame = frame;
    frame = sut.Bird.frame;
    frame.origin.x = 299;
    frame.origin.y = 500;
    sut.Bird.frame = frame;
    id mock = OCMPartialMock(sut);
    [sut TunnelMoving];
    OCMVerify([mock GameOver]);
}

-(void)testTunnelMoving_BirdIntersectWithTunnelBottom_ShouldCallGameOver{
    CGRect frame = sut.TunnelBottom.frame;
    frame.origin.x = 300;
    frame.origin.y = 500;
    sut.TunnelBottom.frame = frame;
    frame = sut.Bird.frame;
    frame.origin.x = 299;
    frame.origin.y = 500;
    sut.Bird.frame = frame;
    id mock = OCMPartialMock(sut);
    [sut TunnelMoving];
    OCMVerify([mock GameOver]);
}

-(void)testTunnelMoving_BirdIntersectWithTop_ShouldCallGameOver{
    [sut.Bird setFrame:CGRectMake(sut.Bird.frame.origin.x, sut.Top.frame.origin.y - sut.Bird.frame.size.height, sut.Bird.frame.size.width, sut.Bird.frame.size.height)];
    id mock = OCMPartialMock(sut);
    [sut TunnelMoving];
    OCMVerify([mock GameOver]);
}

-(void)testTunnelMoving_BirdIntersectWithBottom_ShouldCallGameOver{
    [sut.Bird setFrame:CGRectMake(sut.Bird.frame.origin.x, sut.Bottom.frame.size.height, sut.Bird.frame.size.width,sut.Bird.frame.size.height)];
    id mock = OCMPartialMock(sut);
    [sut TunnelMoving];
    OCMVerify([mock GameOver]);
}

-(void)testTunnelMoving_TunnelTopCenterXLesserThanPlus28_ShouldCallPlaceTunnels{
    CGRect frame = sut.TunnelTop.frame;
    frame.origin.x = -28;
    [sut.TunnelTop setFrame:frame];
    id mock = OCMPartialMock(sut);
    [sut TunnelMoving];
    OCMVerify([mock PlaceTunnels]);
}

-(void)testTunnelMoving_TunnelTopCenterXEqual30_ShouldCallScore{
    CGRect frame = sut.TunnelTop.frame;
    frame.origin.x = 31;
    [sut.TunnelTop setFrame:frame];
    id mock = OCMPartialMock(sut);
    [sut TunnelMoving];
    OCMVerify([mock Score]);
}

@end
