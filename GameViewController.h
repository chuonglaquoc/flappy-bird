//
//  GameViewController.h
//  Flappy Brid
//
//  Created by Admin on 3/10/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GameViewController : UIViewController
{
    double RAD;
}
@property (nonatomic) int BirdFlight;
@property (nonatomic) int RandomTopTunnelPosition;
@property (nonatomic) int RandomBottomTunnelPosition;
@property (nonatomic) int ScoreNumber;
@property (nonatomic) NSInteger HighScoreNumber;

@property (nonatomic,strong) IBOutlet UIImageView *Bird;
@property (nonatomic,strong)IBOutlet UIButton *StartGame;

@property (nonatomic,strong)IBOutlet UIImageView *TunnelTop;
@property (nonatomic,strong)IBOutlet UIImageView *TunnelBottom;
@property (nonatomic,strong)IBOutlet UIImageView *Top;
@property (nonatomic,strong)IBOutlet UIImageView *Bottom;

@property (nonatomic,strong)IBOutlet UIButton *Exit;
@property (nonatomic,strong)IBOutlet UILabel *ScoreLabel;

@property (nonatomic,strong) NSTimer *BirdMovement;
@property (nonatomic,strong) NSTimer *TunnelMovement;

-(IBAction)StartGame:(id)sender;
-(void)BirdMoving;
-(void)TunnelMoving;
-(void)PlaceTunnels;
-(void)Score;
-(void)GameOver;

@end
