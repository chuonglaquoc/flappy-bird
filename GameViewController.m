//
//  GameViewController.m
//  Flappy Brid
//
//  Created by Admin on 3/10/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "GameViewController.h"

@interface GameViewController ()

@end

@implementation GameViewController

-(IBAction)StartGame:(id)sender{
    [self TunnelTop].hidden = NO;
    [self TunnelBottom].hidden = NO;
    [self StartGame].hidden = YES;
    
    self.BirdMovement = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(BirdMoving) userInfo:nil repeats:YES];

    [self PlaceTunnels];
    
    self.TunnelMovement = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(TunnelMoving) userInfo:nil repeats:YES];
}

-(void)TunnelMoving{
    [self TunnelTop].center = CGPointMake([self TunnelTop].center.x - 1, [self TunnelTop].center.y);
    [self TunnelBottom].center = CGPointMake([self TunnelBottom].center.x - 1, [self TunnelBottom].center.y);
    
    if ([self TunnelTop].center.x < -28) {
        [self PlaceTunnels];
    }
    
    if ([self TunnelTop].center.x == 30) {
        [self Score];
    }
    
    if (CGRectIntersectsRect([self Bird].frame, [self TunnelTop].frame)) {
        NSLog(@"intersect with tunnel top");
        [self GameOver];
    }
    
    if (CGRectIntersectsRect([self Bird].frame, [self TunnelBottom].frame)) {
        NSLog(@"intersect with tunnel bottom");
        [self GameOver];
    }
    
    if (CGRectIntersectsRect([self Bird].frame, [self Top].frame)) {
        NSLog(@"intersect with top");
        [self GameOver];
    }
    
    if (CGRectIntersectsRect([self Bird].frame, [self Bottom].frame)) {
        NSLog(@"intersect with bottom");
        [self GameOver];
    }
}

-(void)GameOver{
    if (self.ScoreNumber > self.HighScoreNumber) {
        [[NSUserDefaults standardUserDefaults] setInteger:self.ScoreNumber forKey:@"HighScoreSaved"];
    }
    
    [self.TunnelMovement invalidate];
    [self.BirdMovement invalidate];
    
    [self Exit].hidden = NO;
    [self TunnelTop].hidden = YES;
    [self TunnelBottom].hidden = YES;
    [self Bird].hidden = YES;
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)Score{
    self.ScoreNumber = self.ScoreNumber +  1;
    [self ScoreLabel].text = [NSString stringWithFormat:@"%i",self.ScoreNumber];
}

-(void)PlaceTunnels{
    self.RandomTopTunnelPosition = arc4random() % 350;//350
    self.RandomTopTunnelPosition = self.RandomTopTunnelPosition - 228;//228
    self.RandomBottomTunnelPosition = self.RandomTopTunnelPosition + 655;//655
    
    [self TunnelTop].center = CGPointMake(340, self.RandomTopTunnelPosition - 60);
    [self TunnelBottom].center = CGPointMake(340, self.RandomBottomTunnelPosition);
    
}

-(void)BirdMoving{
    self.Bird.center = CGPointMake(self.Bird.center.x, self.Bird.center.y - self.BirdFlight);
    self.BirdFlight = self.BirdFlight - 5;
    
    if (self.BirdFlight < -15) {
        self.BirdFlight = -15;
    }
    
    if (self.BirdFlight > 0) {
        self.Bird.image = [UIImage imageNamed:@"BirdUp.png"];
    }
    if (self.BirdFlight < 0) {
        self.Bird.image = [UIImage imageNamed:@"BirdDown.png"];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    self.BirdFlight = 30;
}

- (void)viewDidLoad {
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"HighScoreSaved"];
    self.TunnelTop.hidden = YES;
    self.TunnelBottom.hidden = YES;
    
    self.Exit.hidden = YES;
    self.ScoreNumber = 0;
    
    self.HighScoreNumber = [[NSUserDefaults standardUserDefaults] integerForKey:@"HighScoreSaved"];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
